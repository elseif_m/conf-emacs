(eval-after-load "warnings"
  '(setq display-warning-minimum-level :error))

(load-file "~/.emacs.d/auto-complete.el")
(add-to-list 'load-path "~/.emacs.d/tools")

(require 'auto-complete)
(require 'auto-complete-words)

(global-auto-complete-mode t)

;;Les headers
(load-file "~/.emacs.d/std.el")
(load-file "~/.emacs.d/std_comment.el")

;; Les skeleton
(load-file "~/.emacs.d/skeleton.el")

;; Pour le respet de la norme
(load-file "~/.emacs.d/norme.el")

;; Les raccourcis
(load-file "~/.emacs.d/raccourcis.el")

;; Les truc utiles
(load-file "~/.emacs.d/utils.el")

(defun pi-indent-whole-buffer ()
  "Indent the whole buffer without change the point."
  (interactive)
  (if (string-match "\\.c$" (buffer-file-name))
      (indent-region (point-min) (point-max) nil)))
(global-set-key (kbd "<C-S-iso-lefttab>") 'pi-indent-whole-buffer)

;;Supression espacs en fin de ligne
(add-hook 'before-save-hook 'whitespace-cleanup)

;;Tabulation auto a la sauvegarde
(add-hook 'before-save-hook 'pi-indent-whole-buffer)

;;AUTO INSERT
(add-hook 'find-file-hooks
	  (lambda ()
	    (when (and (memq major-mode '(c-mode c++-mode)) (equal (point-min) (point-max)) (string-match ".*\\.cc?" (buffer-file-name)))
	      (std-file-header)
	      (goto-line 11)
	      )
	    ))


(defun xah-syntax-color-hex ()
  "Syntax color hex color spec such as 「#ff1100」 in current buffer."
  (interactive)
  (font-lock-add-keywords
   nil   '(("#[abcdef[:digit:]]\\{6\\}"
	    (0 (put-text-property
		(match-beginning 0)
		(match-end 0)
		'face (list :background (match-string-no-properties 0)))))))
  (font-lock-fontify-buffer)
  )

(add-hook 'css-mode-hook 'xah-syntax-color-hex)
(add-hook 'php-mode-hook 'xah-syntax-color-hex)
(add-hook 'html-mode-hook 'xah-syntax-color-hex)

;;OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO;
(setq emacs-lisp-dir "~/.emacs.d/"
      my-elmode-dir (concat emacs-lisp-dir "elmodes/"))
(setq load-path
      (append load-path (list my-elmode-dir)))

(column-number-mode 1)
(line-number-mode 1)

(transient-mark-mode t)

(load-library "paren")
(show-paren-mode 1)

;; Colorise en Rouge les espace
(setq show-trailing-whitespace t)
(setq-default show-trailing-whitespace t)

					;(load-theme 'odersky t)
					;(load-theme 'badger t)
;(load-theme 'firecode t)
					;(load-theme 'ample-zen t)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight bold :height 98 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(mark-even-if-inactive t)
 '(safe-local-variable-values (quote ((company-clang-arguments "-I/home/mechar_b/BitBucket/gomoku/includes/") (eval when (require (quote rainbow-mode) nil t) (rainbow-mode 1)) (folded-file . t) (byte-compile-compatibility . t))))
 '(scroll-bar-mode (quote right))
 '(show-paren-mode t)
 '(transient-mark-mode 1))
