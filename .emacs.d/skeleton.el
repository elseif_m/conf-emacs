;; Skeleton des headers protege

(define-skeleton insert-protect-header
  "Inserts a define to protect the header file."
  ""
  '(std-file-header)
  '(setq str (file-name-sans-extension
	      (file-name-nondirectory (buffer-file-name))))
  "\n#ifndef "(upcase str)"_H_\n"
  "# define "(upcase str)"_H_\n"
  "\n"
  "\n"
  "\n"
  "#endif /* !"(upcase str)"_H_ */\n"
  '(goto-line 14))

;; Skeleton des Makefiles
(define-skeleton create-makefile
  "Create a Makefile."
  ""
  '(std-file-header)
  "\n"
  "CC	= gcc\n"
  "\n"
  "NAME	= \n"
  "\n"
  "CFLAGS	= -Wall -Werror -Wextra\n"
  "CFLAGS	+= -Iinclude -O3\n"
  "CDEBUG	= -g3 -g\n"
  "\n"
  "LDFLAGS	= -Llib/ -lmy\n"
  "\n"
  "SRCS	=\n"
  "\n"
  "OBJS	= $(SRCS:.c=.o)\n"
  "\n"
  "MAKELIB	= make -Clib\n"
  "\n"
  "RM	= rm -f\n"
  "ECHO	= echo -e\n"
  "\n"
  "all : $(NAME)\n"
  "\n"
  "$(NAME) : $(OBJS)\n"
  "	$(MAKELIB)\n"
  "	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)\n"
  "	$(ECHO) '\\033[0;32m> Compiled\\033[0m'\n"
  "\n"
  "clean :\n"
  "	$(MAKELIB) clean\n"
  "	$(RM) $(OBJS)\n"
  "	$(ECHO) '\\033[0;35m> Directory cleaned\\033[0m'\n"
  "\n"
  "fclean : clean\n"
  "	$(MAKELIB) fclean\n"
  "	$(RM) $(NAME)\n"
  "	$(ECHO) '\\033[0;35m> Remove executable\\033[0m'\n"
  "\n"
  "debug : $(OBJS)\n"
  "	$(MAKELIB)\n"
  "	$(CC) $(CDEBUG) $(OBJS) -o $(NAME)  $(LDFLAGS)\n"
  "	$(ECHO) '\\033[0;32m> Mode Debug: done\\033[0m'\n"
  "\n"
  "re : fclean all\n"
  "\n"
  ".PHONY : all clean fclean debug re\n"
  "\n"
  '(goto-line 13)
  '(move-to-column 10))

;;Skeleton des Main
(define-skeleton main
  "Create a main."
  ""
  "\n"
  "int		main(int ac, char **av)\n"
  "{\n"
  "  \n"
  "  return (0);\n"
  "}\n"
  '(goto-line 13)
  '(move-to-column 2))

;; Si Makefile vide, creer un makefile
(add-hook 'find-file-hook
	  (lambda()
	    (if (and
		 (string-match "\\Makefile$" (buffer-file-name))
		 (= (buffer-size) 0))
		(create-makefile))))

;; Si header vide, creer un header protege
(add-hook 'find-file-hook
	  (lambda()
	    (if (and
		 (string-match "\\.h$" (buffer-file-name))
		 (= (buffer-size) 0))
		(insert-protect-header))))

;; Si main.c vide, creer un main.c
(add-hook 'find-file-hook
	  (lambda()
	    (if (and
		 (string-match "main.c$" (buffer-file-name))
		 (= (buffer-size) 0))
		(main))))
